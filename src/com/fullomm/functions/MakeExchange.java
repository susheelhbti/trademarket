/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fullomm.functions;

import com.fullomm.model.Exchange;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;


/**
 *
 * @author saksham
 */
public class MakeExchange {
    // send bitcoin_BTC  ,  ethereum_ETH
     // receive bitcoin_BTC  ,  ethereum_ETH
    //receive account/address the funds will be sent to
    
    public Exchange StartExchange(String email , String send ,String receive ,String amount ,String receiver_id   ) throws IOException{
  
        
        try {
            String parameter="email="+email+"&send="+send+"&receive="+receive+"&amount="+amount+"&receiver_id="+receiver_id;
            
            CommonFunctions cf = new CommonFunctions();
            String res=  cf.wget("http://sakshamapp.com/api-php/call.php?"+parameter);
            
            System.out.println(res);
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(res);
            JSONObject jsonObject = (JSONObject) obj;
            
            
            String exchange_id = (String) jsonObject.get("exchange_id");
            
            System.out.println(exchange_id);
            
            
            Exchange xc= new Exchange();
            
            xc.exchange_id=(String) jsonObject.get("exchange_id");
            
            xc.payee =(String) jsonObject.get("payee");
            xc.send_amount =Objects.toString(jsonObject.get("send_amount"));
            
            return xc;
        } catch (ParseException ex) {
            Logger.getLogger(MakeExchange.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
       
    
    }
    
    
       public Exchange checkExchangeStatus(String exchangeID ) throws JSONException, ParseException, IOException {
      String parameter = "exchangeID=" + exchangeID   ;

        CommonFunctions cf = new CommonFunctions();
        String res = cf.wget("http://sakshamapp.com/api-php/ExchangeStatus.php?" + parameter);

        System.out.println(res);
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(res);
        JSONObject jsonObject = (JSONObject) obj;

      

        Exchange xc = new Exchange();

        xc.status = (String) jsonObject.get("status");

        xc.exchange_time = (String) jsonObject.get("exchange_time");
        xc.send_amount = (String) jsonObject.get("send_amount");

        return xc;
    }
}

         