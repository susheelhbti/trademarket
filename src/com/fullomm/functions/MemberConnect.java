/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fullomm.functions;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.fullomm.model.User;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.simple.parser.ParseException;

/**
 *
 * @author saksham
 */
public class MemberConnect {

    public User checkLogin(String username, String password) throws JSONException, ParseException {
        try {
            //EncryptDecrypt ed = new EncryptDecrypt();

            //String hash = ed.MD5(username + password);
//http://104.155.174.157:8080/fullom/Login?username=admin&password=!@#$%
            CommonFunctions cf = new CommonFunctions();
            String res = cf.wget("http://104.198.72.224:8080/fullom/LoginApi?username=" + username + "&password=" + password);

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(res);
            JSONObject jsonObject = (JSONObject) obj;

            String error = (String) jsonObject.get("Error");

            if ("False".equals(error)) {
                User u = new User();
                u.checkLogin = true;
                u.log = "true";
                System.out.print(41);
                // u.username = (String) jsonObject.get("error");
                //  u.displayName = (String) jsonObject.get("displayName");

                //  u.countryCode = (String) jsonObject.get("countryCode");
                // u.stateProvince = (String) jsonObject.get("stateProvince");
                // u.username = (String) jsonObject.get("username");
                // u.disabled = (boolean) jsonObject.get("disabled");
                u.error = (String) jsonObject.get("Error");
                u.message = (String) jsonObject.get("Message");
                u.name = (String) jsonObject.get("Name");
                u.username = username;
                u.password = password;
                u.message = password;
                System.out.print(u.name);
                return u;

            } else {
                System.out.print(61);
                User u = new User();
                u.log = "false";
                u.checkLogin = false;
                return u;

            }

        } catch (IOException ex) {
            Logger.getLogger(RateFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public User Register(String username, String password, String email, String mobile) throws JSONException, ParseException {
        try {
            //EncryptDecrypt ed = new EncryptDecrypt();

            //String hash = ed.MD5(username + password);
//http://104.155.174.157:8080/fullom/Login?username=admin&password=!@#$%
            System.out.print("http://104.198.72.224:8080/fullom/register?username=" + username + "&password=" + password + "&email=" + email + "&mobile=" + mobile);
            CommonFunctions cf = new CommonFunctions();
            String res = cf.wget("http://104.198.72.224:8080/fullom/register?username=" + username + "&password=" + password + "&email=" + email + "&mobile=" + mobile);

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(res);
            JSONObject jsonObject = (JSONObject) obj;

            //   String error = (String) jsonObject.get("Error");
            String error = (String) jsonObject.get("Error");

            if ("False".equals(error)) {
                User u = new User();
                u.checkRegister = true;
                u.reg = "true";
                // u.username = (String) jsonObject.get("error");
                //  u.displayName = (String) jsonObject.get("displayName");

                //  u.countryCode = (String) jsonObject.get("countryCode");
                // u.stateProvince = (String) jsonObject.get("stateProvince");
                // u.username = (String) jsonObject.get("username");
                // u.disabled = (boolean) jsonObject.get("disabled");
                u.error = (String) jsonObject.get("Error");
                u.balance = (String) jsonObject.get("Balance");
             
                System.out.print(u.balance);
                return u;

            } else {

                User u = new User();
                u.reg = "false";
                u.checkRegister = false;
                return u;

            }

        } catch (IOException ex) {
            Logger.getLogger(RateFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public User GetAddress(String username, String password) throws JSONException, ParseException {
        try {
            //EncryptDecrypt ed = new EncryptDecrypt();

            //String hash = ed.MD5(username + password);
//http://104.155.174.157:8080/fullom/Login?username=admin&password=!@#$%
            System.out.print("http://104.198.72.224:8080/fullom/GetAddress?username=" + username + "&password=" + password);
            CommonFunctions cf = new CommonFunctions();
            String res = cf.wget("http://104.198.72.224:8080/fullom/GetAddress?username=" + username + "&password=" + password);

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(res);
            JSONObject jsonObject = (JSONObject) obj;

            //   String error = (String) jsonObject.get("Error");
            String error = (String) jsonObject.get("Error");

            if ("False".equals(error)) {
                User u = new User();
                u.checkRegister = true;
                u.add = "true";
                // u.username = (String) jsonObject.get("error");
                //  u.displayName = (String) jsonObject.get("displayName");

                //  u.countryCode = (String) jsonObject.get("countryCode");
                // u.stateProvince = (String) jsonObject.get("stateProvince");
                // u.username = (String) jsonObject.get("username");
                // u.disabled = (boolean) jsonObject.get("disabled");
                u.error = (String) jsonObject.get("Error");
                u.message = (String) jsonObject.get("Message");
                u.label = (String) jsonObject.get("Label");
                u.username = username;
                u.password = password;
                System.out.print(u.message);
                return u;

            } else {

                User u = new User();
                u.add = "false";
                u.checkRegister = false;
                return u;

            }

        } catch (IOException ex) {
            Logger.getLogger(RateFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public User send(String username,String address_to,String amount,String fees,String description) throws JSONException, ParseException {
        try {

            System.out.print("http://104.198.72.224:8080/fullom/SendBittcoinapi?username=" + username + "&address_to=" + address_to+"&amount=" + amount+"&fees=" + fees+"&description=" + description);
            CommonFunctions cf = new CommonFunctions();
            String res = cf.wget("http://104.198.72.224:8080/fullom/SendBittcoinapi?username=" + username + "&address_to=" + address_to+"&amount=" + amount+"&fees=" + fees+"&description=" + description);

            JSONParser parser = new JSONParser();
            Object obj = parser.parse(res);
            JSONObject jsonObject = (JSONObject) obj;
            String error = (String) jsonObject.get("Error");
            if ("False".equals(error)) {
                User u = new User();
                u.checkRegister = true;
                u.send = "true";
                
                u.error = (String) jsonObject.get("Error");
                u.message = (String) jsonObject.get("Message");
              System.out.print(194);
                System.out.print(u.message);
                return u;

            } else {

                User u = new User();
                u.send = "false";
                u.checkRegister = false;
                return u;

            }

        } catch (IOException ex) {
            Logger.getLogger(RateFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    
      public User balance(String username) throws JSONException, ParseException {
        try {

            System.out.print("http://104.198.72.224:8080/fullom/BalanceApi?username=" + username );
            CommonFunctions cf = new CommonFunctions();
            String res = cf.wget("http://104.198.72.224:8080/fullom/BalanceApi?username=" + username);
            System.out.print(res);
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(res);
            JSONObject jsonObject = (JSONObject) obj;
            String error = (String) jsonObject.get("Error");
            if ("False".equals(error)) {
                User u = new User();
                u.checkRegister = true;
                u.bal = "true";
                
                u.error = (String) jsonObject.get("Error");
                u.balance = (String) jsonObject.get("Balance");
                u.usd=(String) jsonObject.get("Balance_USD");
             System.out.print(233);
                System.out.print(u.balance);
                return u;

            } else {

                User u = new User();
                u.bal = "false";
                u.checkRegister = false;
                return u;

            }

        } catch (IOException ex) {
            Logger.getLogger(RateFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

      
      
       public User email(String maill) throws JSONException, ParseException {
        try {

            System.out.print("http://104.198.72.224:8080/fullom/EmailApi?email=" + maill);
            CommonFunctions cf = new CommonFunctions();
            String res = cf.wget("http://104.198.72.224:8080/fullom/EmailApi?email=" + maill);
            System.out.print(res);
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(res);
            JSONObject jsonObject = (JSONObject) obj;
            String error = (String) jsonObject.get("Error");
            if ("False".equals(error)) {
                User u = new User();
                u.checkRegister = true;
                u.em = "true";
                
                u.error = (String) jsonObject.get("Error");
                u.response = (String) jsonObject.get("Balance");
             System.out.print(233);
                System.out.print(u.balance);
                return u;

            } else {

                User u = new User();
                u.em = "false";
                u.checkRegister = false;
                return u;

            }

        } catch (IOException ex) {
            Logger.getLogger(RateFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
       
        public User transaction(String user) throws JSONException, ParseException {
            System.out.print("http://104.198.72.224:8080/fullom/Transaction?username=" + user);
            MemberConnect cf = new MemberConnect();
            String res = cf.wgett("http://104.198.72.224:8080/fullom/Transaction?username=" + user);
            System.out.print(res);
            System.out.print("trans");
            User u = new User();
            u.checkRegister = true;
            u.trans = "true";
            u.data=res;
            return u;
            
            /*JSONParser parser = new JSONParser();
            Object obj = parser.parse(res);
            JSONObject jsonObject = (JSONObject) obj;
            String error = (String) jsonObject.get("Error");
            if ("False".equals(error)) {
            User u = new User();
            u.checkRegister = true;
            u.em = "true";
            
            u.error = (String) jsonObject.get("Error");
            u.response = (String) jsonObject.get("Balance");
            System.out.print(233);
            System.out.print(u.balance);
            return u;
            
            } else {
            
            User u = new User();
            u.em = "false";
            u.checkRegister = false;
            return u;
            
            }*/


    }
        
        public String wgett( String url)   {
          
        try {
            System.out.println(url);
              URL u  = new URL(url);
                    HttpURLConnection conn = (HttpURLConnection) u.openConnection();
                    conn.setRequestMethod("GET");
                    //conn.setRequestProperty("Accept", "application/json");

                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Failed : HTTP error code : "
                                + conn.getResponseCode());
                    }

                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            (conn.getInputStream())));

                    String output;
                    String line;
                    System.out.println("88");
                    output = br.readLine();
                    
                       while ((line = br.readLine()) != null)
      {
        output=output+line  ;
      }
                       
                       
                       
                    
                    return output;
                    
        } catch (Exception ex) {
           // Logger.getLogger(MarketData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
                
    }
}
